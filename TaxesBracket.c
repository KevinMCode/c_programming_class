#include <stdio.h>

int main (void)

{
    const int tax1 = 20; // Tax rate for first bracket.
    const int tax2 = 22; // Tax rate for second bracket.
    const int tax3 = 25; // Tax rate for third bracket.
    const int tierOne = 300; // Ceiling for first bracket.
    const int tierTwo = 450; // Ceiling for second bracket.
    int fullTime;
    float tax;
    float netIncome; // I tried to use as many variable and constants as possible
    float pastTierOne;  // so you can change constants (parameters) it will still work.
    float pastTierTwo;
    float hoursWorked;
    int overTime;
    float basePay = 16.00;
    float weekPay;
    fullTime = 40;

    pastTierOne = tierOne * (tax1 *.01); // Maximum tax for the first bracket (whatever it is)
    pastTierTwo = (tierOne * (tax1 * .01)) + ((tierTwo - tierOne) * (tax2 * .01));
    // Total of maximum taxes for first and second bracket (whatever it is)
    printf("How many hours did you work this week: ");
    scanf("%f", &hoursWorked);

    if (hoursWorked > fullTime) // If worked more that full time... execute
    {                           // else go to else statement
        overTime = hoursWorked - fullTime;
        weekPay = (fullTime + (overTime * 1.5)) * basePay;
        if (weekPay <= tierOne)
        {
            tax = (weekPay * tax1)/100;
        }
        else if (weekPay <= tierTwo)
        {
            tax = pastTierOne + ((weekPay - tierOne) * tax2)/100;
        }   // Maximum of money in first bracket + remainder * rate
        else if (weekPay > tierTwo)
        {
            tax = pastTierTwo + ((weekPay - tierTwo) * tax3)/100;
        }   // Maximum of money in first two brackets + remainder * rate
        printf("Your gross pay is $%.2f\n", weekPay);
        printf("Your taxes are $%.2f\n", tax);
        netIncome = weekPay - tax;
        printf("Your net income is $%.2f\n", netIncome);
    }
    else // If you did not work full time run this
    {
        weekPay = hoursWorked * basePay;
        if (weekPay <= tierOne)
        {
            tax = (weekPay * tax1)/100;
        }
        else if (weekPay <= tierTwo)
        {
            tax = pastTierOne + ((weekPay - tierOne) * tax2)/100;
        }
        else if (weekPay > tierTwo)
        {
            tax = pastTierTwo + ((weekPay - tierTwo) * tax3)/100;
        }
        printf("Your gross pay is $%.2f\n", weekPay);
        printf("Your taxes are $%.2f\n", tax);
        netIncome = weekPay - tax;
        printf("Your net income is $%.2f\n", netIncome);
    }
    return 0;
}
