/* This is a C program that accepts integers until 0 is entered. After input terminates,
the program should report the total number of even integers (excluding 0) , the 
average value of the even integers, the total number of odd integers, and the average
value of the odd integers. The program should also report "EVEN WINS!",  "ODD WINS!",
or "IT'S A TIE!" based on which total was greater. Test your program thoroughly and
submit output with at least ten (10) integers as input.*/

#include <stdio.h>
#define MULTIPLE 2
int main()

{   int even = -1;
    int odd = 0;
    int number;
    float sumodd = 0;
    float oddAvg, evenAvg;
    float sumeven = 0;


    printf("I will total number of even integers, excluding 0,\n");
    printf("give the average value of the even integers,\n");
    printf("give the total number of odd integers,\nand the average value ");
    printf("of the odd integers.\n");
    printf("Till you give me a 0 then I will show the results\n");
    
    do
    {
        printf("Enter an integer: ");
        scanf("%d", &number);

        if( number%MULTIPLE )
        {
            odd++;           //OK its odd... then add one to the odd count
            sumodd += number;//add the number you just got (number) to the sum
            continue;        //of the numbers (sumodd) you are accumulating.
        }
        else
        {
            even++;
            sumeven += number; // of its even... same logic as above
        }
    }
    while(number != 0); // while number is not 0 Yaba Daba DOOOO!!

    oddAvg = sumodd / odd; // sum of odd numbers divided by odd count
    evenAvg = sumeven / even;

    printf("Number of Odd Numbers = %d\n", odd);
    printf("Number of Even Numbers = %d\n", even);

    printf("Odd Average = %.2lf\n",oddAvg);
    printf("Even Average = %.2lf\n",evenAvg);

    if (odd > even)
        {
        printf("ODD WINS!!\n");
        return 0; // We found the winner... EOF.
        }
    else if (even > odd)
        {
        printf("EVEN WINS!!\n");
        return 0;
        }
    else (even = odd);
        {
        printf("IT'S A TIE!!\n");
        return 0;
        }
    }


