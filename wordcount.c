// wordcount.c -- The program will report the number of words
// and the average number of letters per word.
#include <stdio.h>
#include <ctype.h> // for isspace()
#include <stdbool.h> // for bool, true, false
#include <stdlib.h> // for exit(1) in case file not found
#define ARRAYSIZE 15
    int main(void)
    {
        char character; // read in character
        int uppercase, lowercase;
        float avgNumLtrs;
        char prev; // previous character read
        long numLetters = 0L; // number of letters
        int numWords = 0; // number of words
        bool inword = false;// == true if c is in a word
        FILE * fContent;
        char fileName[ARRAYSIZE] = "infile.txt";

        fContent = fopen(fileName, "r"); // open file for reading
    if (fContent == NULL)// attempt failed
    {
        printf("Failed to open file. Please try again.\n");
        exit(1); // quit program
    }
    // getc(fContent) gets a character from the open file till EOF
    while ((character = getc(fContent)) != EOF)
            {
            if (character >= 'A' && character <= 'Z') // Finding
            {            // ONLY uppercase letters (ASCII characters)
            uppercase++;
            }
            if (character >= 'a' && character <= 'z')// Finding
            {            // ONLY lowercase letters (ASCII characters)
            lowercase++;
            }
            if (!isspace(character) && !inword)
                {
                inword = true; // starting a new word
                numWords++; // count word
                }
            if (isspace(character) && inword)
                {
                inword = false; // reached end of word
                prev = character; // save character value
                }
        }

        numLetters = lowercase + uppercase; // ACTUAL LETTERS ONLY!!!!

        avgNumLtrs = ((float)numLetters / (float)numWords);
        printf("There are %d words in the text file\n",numWords);
        printf("The average number of letters in %d word(s) is %.3f\n"
        , numWords, avgNumLtrs);

        return 0;
}
