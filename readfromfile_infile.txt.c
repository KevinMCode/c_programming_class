// p8.c --open a file and display the number of uppercase characters,
// the number of lowercase characters, and the number of other characters read
// from the file infile.txt from the same folder
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define ARRAYSIZE 15
int main()
{
    int character;
    FILE * fContent;
    char fileName[ARRAYSIZE] = "infile.txt"; // the file name you are getting
    int uppercase = 0;                      // input from.
    int lowercase = 0;
    int other = 0;

    fContent = fopen(fileName, "r"); // open file for reading
    if (fContent == NULL)// attempt failed
    {
        printf("Failed to open file. Please try again.\n");
        exit(1); // quit program
    }
    // getc(fContent) gets a character from the open file runs it through the if-else
    // filter till EOF
    while ((character = getc(fContent)) != EOF)
    {
        if (character >= 'A' && character <= 'Z') // if capital A through Z count 1
        {
            uppercase++;
        }
        else if (character >= 'a' && character <= 'z') // if lowercase a through z
        {                                               // count 1
            lowercase++;
        }
        else if (character !=32 && character !=13 && character !=10) // if not a space,
        {                            // not a carriage return, and not a new line feed
            other++;
        }
        else // if you are those others, I don't care... continue
        {
            continue;
        }
    }

    printf("There are %d uppercase letters\n", uppercase);
    printf("There are %d lowercase letters\n", lowercase);
    printf("There are %d other characters\n", other);

    fclose(fContent); // close the file
    return 0;
}
