/* CProgFinalProject.c Here is a foolproof (with error handling for the small kids)
program that will print a Tic-Tac-Toe board, personalize the program by asking
for a name, what character other than a number they would like to use to fill their
square, and prompt each player back and forth to select a square on the board by
selecting the number in the square. Once the square is selected the program will
check if the square is already selected, the number you picked is on the board,
then check if there is a winner yet. This will continue till there is a winner. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define SIZE 4
#define S 10

void TTT_squares(char squares[SIZE][SIZE]);
int winLook(char squares[SIZE][SIZE]);
int clean_stdin(void);

int main(void)
{
	int a = 0, d = 0, player = 0, first = 0, over = 0, down = 0, champ = 0;
	int i = 0, j = 0;
	char newGame = 'q';
	char tttBoard[SIZE][SIZE];
	char name1[25];
	char name2[25];
	char one1[S], one;
	char two2[S], two;
	char c, b, error;
	printf("Player 1 please enter your first name: ");
	scanf("%s", &name1);
	printf("\nPlayer 2 please enter your first name: ");
	scanf("%s", &name2);
	do
	{
		printf("\n%s, I will use the first character  \n"
			"other than a number to fill your square, \n"
			"please enter your character: ", name1);
		scanf("%s", &one1);
		one = one1[0];

	} while (isdigit(one));

	do
	{
		printf("\n%s, please use a different character. \n"
			"I will use the first character  \n"
			"other than a number to fill your square, \n"
			"please enter your character: ", name2);
		scanf("%s", &two2[0]);
		two = two2[0];

	} while (isdigit(two) || two == one);

	while (champ == '\0')
	{
		for (a = 0; a < SIZE; a++)
		{
			for (d = 0; d < SIZE; d++)
			{
				tttBoard[a][d] = '\0';
			}
		}
		for (a = 0; a < (SIZE * SIZE) && champ == 0; a++)
		{
			system("cls");
			do
			{
				system("cls");
				TTT_squares(tttBoard);
				player = a % 2 + 1;
				if (player == 1)
				{
					printf("\n\n %s, please enter the number of the "
						"square where you want to place your %c: ", name1, one);
				}
				else if (player == 2)
				{
					printf("\n\n %s, please enter the number of the "
						"square where you want to place your %c: ", name2, two);
				}
			}                                                     // see function below
			while (((scanf("%d%c", &first, &error) != 2 || error != '\n') && clean_stdin()) || first<1 || first>16);
			// The line above is to keep the program from going in an infinite loop if the user decides
			// to enter anything (including letters) that is not a number between 1-16
			first--;
			down = first%SIZE;
			first = first - down;
			over = first / SIZE;

			if (first < 0 || first >(SIZE * SIZE) || tttBoard[over][down] == one || tttBoard[over][down] == two)
			{
				printf("Space is already taken, please try again...");
				getchar();
				a--;
			}
			else
			{
				tttBoard[over][down] = (player == 1) ? one : two;
			}
			champ = winLook(tttBoard);
		}
		if (champ != '\0')
		{
			if (champ == one)
			{
				printf("\nThe Champ was %s! Way to go Champ!!", name1);
			}
			if (champ == two)
			{
				printf("\nThe Champ was %s! Way to go Champ!!", name2);
			}
			printf("\n\nDo you want to play again? y/n: ");
			while (!(newGame == 'y' || newGame == 'n'))
			{
				scanf("%c", &newGame);
			}
			if (newGame == 'y')
			{
				champ = '\0';
				newGame = 'q';
			}
		}
		else
		{
			printf("No winner this round. CAT'S GAME!! Try again. ");
			printf("Do you want to play again? y/n: ");
			while (!(newGame == 'y' || newGame == 'n'))
			{
				scanf("%c", &newGame);
			}
			if (newGame == 'y')
			{
				champ = '\0';
				newGame = 'q';
			}
			else
				return 0;
		}
	}
	return 0;
}

int clean_stdin(void)
{
	while (getchar() != '\n');
	printf("Non-integer characters detected. Press enter to try again...");
	getchar();
	system("cls");
	return 1;
}

void TTT_squares(char squares[SIZE][SIZE])
{
	int a, d;
	printf("\n\n");
	for (a = 0; a < SIZE; a++)
	{
		for (d = 0; d < SIZE; d++)
		{
			if (squares[a][d] == '\0')
			{
				printf(" %2d ", SIZE*(a)+(d + 1));
			}
			else
			{
				printf(" %c  ", squares[a][d]);
			}
			if (d != 3)
			{
				printf("|");
			}
		}
		if (a != 3) {
			printf("\n-------------------\n");
		}
	}
}

int winLook(char squares[SIZE][SIZE])
{
	int a, d;
	char present;
	char champ = '\0';
	// Iteration over rows to check for winner
	for (a = 0; a < SIZE; a++)
	{
		present = squares[a][0];
		for (d = 0; d < SIZE; d++)
		{
			if (squares[a][d] != present)
			{
				present = '\0';
			}
		}
		if (present != '\0')
		{
			champ = present;
		}
	}

	// Iteration over column
	for (a = 0; a<SIZE; a++)
	{
		present = squares[0][a];
		for (d = 0; d < SIZE; d++)
		{
			if (squares[d][a] != present)
			{
				present = '\0';
			}
		}
		if (present != '\0')
        {
			champ = present;
		}
	}
	// Iteration over diagonals
	present = squares[0][0];
	for (a = 0; a < SIZE; a++)
	{
		if (squares[a][a] != present)
		{
			present = '\0';
		}
	}
	if (present != '\0')
	{
		champ = present;
	}
	present = squares[0][SIZE];
	for (a = 0; a <SIZE; a++)
	{
		if (squares[a][SIZE - a] != present)
		{
			present = '\0';
		}
	}
	if (present != '\0') {
		champ = present;
	}
	return champ;
}


