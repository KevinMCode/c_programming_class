// p5two.c -- raises number to the 4th power using a function
#include <stdio.h>
double pow4(double b, int e); // ANSI prototype
int main(void)
{
    double base, expFun;
    int exp;
    exp = 4;
    printf("Enter a number you would like \nto be raised ");
    printf("to the 4th power: ");
    scanf("%lf", &base);
    {
        expFun = pow4(base, exp); // calling the function
        printf("%.2g to the %dth power is %.4g\n", base, exp, expFun);
    }
    printf("Thank you for using my calculator program in C!\n");

    return 0;
}
double pow4(double b, int e) // declaring the function
{
double pow;
pow = 1;
int i;
for (i = 1; i <= e; i++)
pow *= b;
return pow; // return the value of pow
}
